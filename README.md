# README #

The mysql Docker image is used for lauching a single mysql instance.
Based on the official mysql image

### Using the equaldog/mysql image ###

    docker pull equaldog/mysql
    docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=mysecretpassword -d mysql

### Using with the equaldog/mysql-data image ###

    docker pull equaldog/mysql-data
    docker run --name mysql-data equaldog/mysql-data echo Data-only container for mysql
    docker run --name some-mysql --volumes-from mysql-data -e MYSQL_ROOT_PASSWORD=mysecretpassword -d mysql


### Connect to the container from an application ###

    docker run --name some-app --link some-mysql:mysql -d application-that-uses-mysql

### Connecting to mysql with mysql-client ###

Getting the container IP:

    docker inspect some-mysql | grep IPAddress

Here's the result:

    "IPAddress": "172.17.0.20",

Accessing mysql:

    mysql -h172.17.0.20 -uroot -pmysecretpassword
